<?php

namespace App\Entity;

use App\Repository\CommentRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
#[ORM\Table(name: 'comments')]
class Comment
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    #[Groups(['public'])]
    private $id;

    #[ORM\Column(type: 'text')]
    #[Assert\NotNull]
    #[Assert\NotBlank]
    #[Assert\Type(type: 'string', message: 'The value {{ value }} is not a valid {{ type }}.')]
    #[Groups(['public'])]
    private string $content;

    #[ORM\Column(type: 'datetime')]
    #[Groups(['public'])]
    private \DateTimeInterface $date;

    #[ORM\ManyToOne(targetEntity: 'User')]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(['public'])]
    private User $owner;

    #[ORM\ManyToOne(targetEntity: 'Comment', inversedBy: 'replies')]
    #[ORM\JoinColumn(nullable: true)]
    #[Groups(['private'])]
    private ?Comment $parent;

    #[ORM\ManyToOne(targetEntity: 'Post', inversedBy: 'comments')]
    #[Groups(['private'])]
    private Post $post;

    #[ORM\OneToMany(targetEntity: 'Comment', mappedBy: 'parent')]
    #[Groups(['public'])]
    private Collection $replies;

    public function __construct()
    {
        $this->replies = new ArrayCollection();
        $this->date = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getOwner(): User
    {
        return $this->owner;
    }

    public function setOwner(User $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?Comment $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    public function getDate(): \DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getReplies(): Collection
    {
        return $this->replies;
    }

    public function setReplies(Collection $replies): self
    {
        $this->replies = $replies;

        return $this;
    }

    public function addReplies(Comment $comment): self
    {
        if (!$this->replies->contains($comment)) {
            $this->replies->add($comment);
            $comment->setParent($comment);
        }

        return $this;
    }

    public function removeReplies(Comment $comment): self
    {
        if ($this->replies->contains($comment)) {
            $this->replies->removeElement($comment);

            $comment->setParent(null);
        }

        return $this;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

    public function setPost(Post $post): Comment
    {
        $this->post = $post;

        return $this;
    }
}
