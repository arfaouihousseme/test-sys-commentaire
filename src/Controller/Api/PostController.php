<?php

namespace App\Controller\Api;

use App\ArgumentResolver\Body;
use App\Exception\NotFoundEntityException;
use App\Factory\CommentFactory;
use App\Factory\PostFactory;
use App\Hydrator\PostHydrator;
use App\Manager\CommentManagerInterface;
use App\Manager\PostManagerInterface;
use App\Request\Api\Comment\CreateCommentRequest;
use App\Request\Api\Post\CreatePostRequest;
use App\Request\Api\Post\UpdatePostRequest;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/posts')]
#[OA\Tag(name: 'Posts')]
#[Security(name: 'Bearer')]
class PostController extends AbstractController
{
    public function __construct(
        private readonly PostManagerInterface $postManager,
        private readonly CommentManagerInterface $commentManager,
        private readonly PostHydrator $postHydrator
    ) {
    }

    #[Route(path: '', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get all posts'
    )]
    public function listItems(): Response
    {
        $posts = $this->postManager->findAll();

        return $this->json($posts, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get post by ID'
    )]
    public function getItem(int $id): Response
    {
        try {
            $post = $this->postManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        return $this->json($post, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '', methods: ['POST'])]
    #[OA\Response(
        response: 201,
        description: 'Create new post'
    )]
    public function createItem(#[Body] CreatePostRequest $createPostRequest): Response
    {
        $post = PostFactory::create(
            title: $createPostRequest->getTitle(),
            content: $createPostRequest->getContent(),
            owner: $this->getUser(),
        );

        $post = $this->postManager->save($post);

        return $this->json($post, Response::HTTP_CREATED, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['PATCH'])]
    #[OA\Response(
        response: 200,
        description: 'update post by ID'
    )]
    public function updateItem(int $id, #[Body] UpdatePostRequest $updatePostRequest): Response
    {
        try {
            $post = $this->postManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $post = $this->postHydrator->hydrate($post, $updatePostRequest->toArray());

        $post = $this->postManager->update($post);

        return $this->json($post, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['DELETE'])]
    #[OA\Response(
        response: 201,
        description: 'Delete post by ID'
    )]
    public function deleteItem(int $id): Response
    {
        try {
            $post = $this->postManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $this->postManager->delete($post);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/{id}/comments', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'List comments of post'
    )]
    public function listCommentItems(int $id): Response
    {
        try {
            $post = $this->postManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $comments = $post->getComments();

        return $this->json($comments, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}/comments', methods: ['POST'])]
    #[OA\Response(
        response: 201,
        description: 'Add comment to post'
    )]
    public function createCommentItemsInPost(int $id, CreateCommentRequest $createCommentRequest): Response
    {
        try {
            $post = $this->postManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $comment = CommentFactory::create(
            content: $createCommentRequest->getContent(),
            owner: $this->getUser(),
            post: $post,
        );

        $comment = $this->commentManager->save($comment);

        return $this->json($comment, Response::HTTP_CREATED, [], ['groups' => ['public']]);
    }
}
