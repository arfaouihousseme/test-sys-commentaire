<?php

namespace App\Controller\Api;

use App\ArgumentResolver\Body;
use App\Exception\NotFoundEntityException;
use App\Factory\CommentFactory;
use App\Hydrator\CommentHydrator;
use App\Manager\CommentManagerInterface;
use App\Manager\LikeManagerInterface;
use App\Request\Api\Comment\CreateCommentRequest;
use App\Request\Api\Comment\UpdateCommentRequest;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/comments')]
#[OA\Tag(name: 'Comments')]
#[Security(name: 'Bearer')]
class CommentController extends AbstractController
{
    public function __construct(
        private readonly CommentManagerInterface $commentManager,
        private readonly LikeManagerInterface $likeManager,
        private readonly CommentHydrator $commentHydrator,
    ) {
    }

    #[Route(path: '', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get all comments'
    )]
    public function listItems(): Response
    {
        $comments = $this->commentManager->findAll();

        return $this->json($comments, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get comment by ID'
    )]
    public function getItem(int $id): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        return $this->json($comment, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['PATCH'])]
    #[OA\Response(
        response: 200,
        description: 'Update comment by ID'
    )]
    public function updateItem(int $id, #[Body] UpdateCommentRequest $updateCommentRequest): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $comment = $this->commentHydrator->hydrate($comment, $updateCommentRequest->toArray());

        $comment = $this->commentManager->update($comment);

        return $this->json($comment, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['DELETE'])]
    #[OA\Response(
        response: 204,
        description: 'Delete comment by ID'
    )]
    public function deleteItem(int $id): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $this->commentManager->delete($comment);

        return $this->json([], Response::HTTP_NO_CONTENT);
    }

    #[Route(path: '/{id}/replies', methods: ['POST'])]
    #[OA\Response(
        response: 201,
        description: 'Add Reply to comment'
    )]
    public function replyToComment(int $id, CreateCommentRequest $createCommentRequest): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $comment = CommentFactory::create(
            content: $createCommentRequest->getContent(),
            owner: $this->getUser(),
            post: $comment->getPost(),
            parent: $comment,
        );

        $comment = $this->commentManager->save($comment);

        return $this->json($comment, Response::HTTP_CREATED, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}/replies', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Add Replies of a comment'
    )]
    public function listRepliesOfComment(int $id): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $replies = $comment->getReplies();

        return $this->json($replies, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}/likes', methods: ['POST'])]
    #[OA\Response(
        response: 201,
        description: 'Add Like to comment'
    )]
    public function likeComment(int $id): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        if ($this->likeManager->findByCommentAndUser($comment, $this->getUser())) {
            throw new ConflictHttpException(sprintf('Like on comment id (%s) already exist', $comment->getId()));
        }

        $like = $this->likeManager->save($comment, $this->getUser());

        return $this->json($like, Response::HTTP_CREATED, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}/likes', methods: ['DELETE'])]
    #[OA\Response(
        response: 204,
        description: 'Remove Like from comment'
    )]
    public function unlikeComment(int $id): Response
    {
        try {
            $comment = $this->commentManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        $like = $this->likeManager->findByCommentAndUser($comment, $this->getUser());

        if ($like) {
            $this->likeManager->delete($like);
        }

        return $this->json([], Response::HTTP_NO_CONTENT);
    }
}
