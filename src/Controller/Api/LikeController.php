<?php

namespace App\Controller\Api;

use App\Exception\NotFoundEntityException;
use App\Manager\LikeManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Attributes as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

#[Route(path: '/api/likes')]
#[OA\Tag(name: 'Likes')]
#[Security(name: 'Bearer')]
class LikeController extends AbstractController
{
    public function __construct(
        private readonly LikeManagerInterface $likeManager,
    ) {
    }

    #[Route(path: '', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get all likes'
    )]
    public function listItems(): Response
    {
        $likes = $this->likeManager->findAll();

        return $this->json($likes, Response::HTTP_OK, [], ['groups' => ['public']]);
    }

    #[Route(path: '/{id}', methods: ['GET'])]
    #[OA\Response(
        response: 200,
        description: 'Get Like by ID'
    )]
    public function getItem(int $id): Response
    {
        try {
            $like = $this->likeManager->findById($id);
        } catch (NotFoundEntityException $exception) {
            throw new NotFoundHttpException($exception->getMessage());
        }

        return $this->json($like, Response::HTTP_OK, [], ['groups' => ['public']]);
    }
}
