<?php

namespace App\Controller\Web;

use App\Manager\CommentManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/')]
class HomePageController extends AbstractController
{
    private const LIMIT_COMMENT = 10;

    public function __construct(private readonly CommentManagerInterface $commentManager)
    {
    }

    #[Route('/', name: 'app_main', methods: ['GET'])]
    public function index(): Response
    {
        $lastComments = $this->commentManager->findAll(self::LIMIT_COMMENT);

        return $this->render('home_page/index.html.twig', [
            'user' => $this->getUser(),
            'comments' => $lastComments,
        ]);
    }
}
