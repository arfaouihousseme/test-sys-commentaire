<?php

namespace App\Factory;

use App\Entity\Post;
use App\Entity\User;

class PostFactory
{
    public static function create(string $title, string $content, User $owner): Post
    {
        $post = new Post();
        $post->setContent($content)
            ->setTitle($title)
            ->setOwner($owner);

        return $post;
    }
}
