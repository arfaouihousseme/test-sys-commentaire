<?php

namespace App\Factory;

use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;

class CommentFactory
{
    public static function create(string $content, User $owner, Post $post, ?Comment $parent = null): Comment
    {
        $comment = new Comment();
        $comment->setContent($content)
            ->setOwner($owner)
            ->setParent($parent)
            ->setPost($post);

        return $comment;
    }
}
