<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Exception\NotFoundEntityException;
use App\Repository\CommentRepository;
use Doctrine\ORM\EntityManagerInterface;

class CommentManager implements CommentManagerInterface
{
    public function __construct(
        private readonly CommentRepository $commentRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function findAll(?int $limit = 20): array
    {
        return $this->commentRepository->findBy([], ['date' => 'desc'], $limit);
    }

    /**
     * @throws NotFoundEntityException
     */
    public function findById(int $id): ?Comment
    {
        $comment = $this->commentRepository->find($id);

        if (!$comment) {
            throw new NotFoundEntityException(self::getEntityName(), $id);
        }

        return $comment;
    }

    public function save(Comment $comment): Comment
    {
        $this->entityManager->persist($comment);
        $this->entityManager->flush();

        return $comment;
    }

    public function update(Comment $comment): Comment
    {
        $this->entityManager->merge($comment);
        $this->entityManager->flush();

        return $comment;
    }

    public function delete(Comment $comment): void
    {
        $this->entityManager->remove($comment);
        $this->entityManager->flush();
    }

    public static function getEntityName(): string
    {
        return 'Comment';
    }
}
