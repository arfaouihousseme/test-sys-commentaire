<?php

namespace App\Manager;

use App\Entity\Post;
use App\Exception\NotFoundEntityException;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;

class PostManager implements PostManagerInterface
{
    public function __construct(
        private readonly PostRepository $postRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function findAll(?int $limit = 20): array
    {
        return $this->postRepository->findBy([], ['id' => 'desc'], $limit);
    }

    /**
     * @throws NotFoundEntityException
     */
    public function findById(int $id): ?Post
    {
        $post = $this->postRepository->find($id);

        if (!$post) {
            throw new NotFoundEntityException(self::getEntityName(), $id);
        }

        return $post;
    }

    public static function getEntityName(): string
    {
        return 'Post';
    }

    public function save(Post $post): Post
    {
        $this->entityManager->persist($post);
        $this->entityManager->flush();

        return $post;
    }

    public function update(Post $post): Post
    {
        $this->entityManager->merge($post);
        $this->entityManager->flush();

        return $post;
    }

    public function delete(Post $post): void
    {
        $this->entityManager->remove($post);
        $this->entityManager->flush();
    }
}
