<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Exception\NotFoundEntityException;

interface CommentManagerInterface
{
    public function findAll(?int $limit = 20): array;

    /**
     * @throws NotFoundEntityException
     */
    public function findById(int $id): ?Comment;

    public function save(Comment $comment): Comment;

    public function update(Comment $comment): Comment;

    public function delete(Comment $comment): void;
}
