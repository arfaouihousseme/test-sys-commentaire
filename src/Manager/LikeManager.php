<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\User;
use App\Exception\NotFoundEntityException;
use App\Repository\LikeRepository;
use Doctrine\ORM\EntityManagerInterface;

class LikeManager implements LikeManagerInterface
{
    public function __construct(
        private readonly LikeRepository $likeRepository,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    public function findAll(): array
    {
        return $this->likeRepository->findAll();
    }

    /**
     * @throws NotFoundEntityException
     */
    public function findById(int $id): ?Like
    {
        $like = $this->like()->find($id);

        if (!$like) {
            throw new NotFoundEntityException(self::getEntityName(), $id);
        }

        return $like;
    }

    public function findByCommentAndUser(Comment $comment, User $user): ?Like
    {
        return $this->likeRepository->findOneBy(['comment' => $comment, 'user' => $user]);
    }

    public function save(Comment $comment, User $user): Like
    {
        $like = new Like();
        $like->setUser($user)
            ->setComment($comment);

        $this->entityManager->persist($like);
        $this->entityManager->flush();

        return $like;
    }

    public function delete(Like $like): void
    {
        $this->entityManager->remove($like);
        $this->entityManager->flush();
    }

    public static function getEntityName(): string
    {
        return 'Like';
    }
}
