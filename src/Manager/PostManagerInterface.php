<?php

namespace App\Manager;

use App\Entity\Post;
use App\Exception\NotFoundEntityException;

interface PostManagerInterface
{
    public function findAll(?int $limit = 20): array;

    /**
     * @throws NotFoundEntityException
     */
    public function findById(int $id): ?Post;

    public function save(Post $post): Post;

    public function update(Post $post): Post;

    public function delete(Post $post): void;
}
