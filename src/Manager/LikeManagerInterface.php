<?php

namespace App\Manager;

use App\Entity\Comment;
use App\Entity\Like;
use App\Entity\User;
use App\Exception\ConflictEntityException;
use App\Exception\NotFoundEntityException;

interface LikeManagerInterface
{
    public function findAll(): array;

    /**
     * @throws NotFoundEntityException
     */
    public function findById(int $id): ?Like;

    public function findByCommentAndUser(Comment $comment, User $user): ?Like;

    /**
     * @throws ConflictEntityException
     */
    public function save(Comment $comment, User $user): Like;

    public function delete(Like $like): void;
}
