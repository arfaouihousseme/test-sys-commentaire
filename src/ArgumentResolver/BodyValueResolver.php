<?php

namespace App\ArgumentResolver;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BodyValueResolver implements ArgumentValueResolverInterface
{
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly ValidatorInterface $validator
    ) {
    }

    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        $type = $argument->getType() ?? 'string';
        $format = $request->getContentTypeFormat() ?? 'json';
        $content = $request->getContent();

        $data = $this->serializer->deserialize($content, $type, $format);

        $violations = $this->validator->validate($data);

        if (count($violations) > 0) {
            throw new BadRequestHttpException($this->violationsToString($violations));
        }

        yield $data;
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        if (!$argument->getType()) {
            return false;
        }

        $attrs = $argument->getAttributes(Body::class);

        return count($attrs) > 0;
    }

    private function violationsToString(ConstraintViolationList $violationsList): string
    {
        $output = '';

        foreach ($violationsList as $violation) {
            $output .= sprintf(
                'Error on flied \'%s\' (%s) ',
                $violation->getPropertyPath(),
                $violation->getMessage(),
            );
        }

        return $output;
    }
}
