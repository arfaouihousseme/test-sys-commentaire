<?php

namespace App\Hydrator;

use App\Entity\Post;

class PostHydrator
{
    public function hydrate(Post $post, array $data): Post
    {
        $fields = array_keys($data);

        foreach ($fields as $field) {
            switch ($field) {
                case 'title':
                    if ($data[$field]) {
                        $post->setTitle($data[$field]);
                    }
                    break;
                case 'content':
                    if ($data[$field]) {
                        $post->setContent($data[$field]);
                    }
                    break;
            }
        }

        return $post;
    }
}
