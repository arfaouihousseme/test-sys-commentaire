<?php

namespace App\Hydrator;

use App\Entity\Comment;

class CommentHydrator
{
    public function hydrate(Comment $comment, array $data): Comment
    {
        $fields = array_keys($data);

        foreach ($fields as $field) {
            switch ($field) {
                case 'content':
                    if ($data[$field]) {
                        $comment->setContent($data[$field]);
                    }
                    break;
            }
        }

        return $comment;
    }
}
