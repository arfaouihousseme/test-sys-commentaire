<?php

namespace App\Exception;

class ConflictEntityException extends \Exception
{
    public function __construct(string $id)
    {
        parent::__construct(sprintf('%s already exist', $id));
    }
}
