<?php

namespace App\Exception;

class NotFoundEntityException extends \Exception
{
    public function __construct(string $entity, string $sourceId)
    {
        parent::__construct(sprintf("'%s' does not exist with this id: '%s'.", $entity, $sourceId));
    }
}
