<?php

namespace App\Request\Api\Post;

use App\Request\Post\UpdateCommentRequest;
use App\Trait\ConvertObjectToArrayTrait;

class UpdatePostRequest
{
    use ConvertObjectToArrayTrait;

    private ?string $title = null;

    private ?string $content = null;

    public static function of(?string $title = null, ?string $content = null): self
    {
        $post = new UpdateCommentRequest();

        $post->setContent($content)
             ->setTitle($title);

        return $post;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): UpdateCommentRequest
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): UpdateCommentRequest
    {
        $this->content = $content;

        return $this;
    }
}
