<?php

namespace App\Request\Api\Post;

use App\Request\Post\CreateCommentRequest;
use Symfony\Component\Validator\Constraints as Assert;

class CreatePostRequest
{
    #[Assert\NotBlank()]
    #[Assert\NotNull()]
    private string $title;

    #[Assert\NotBlank()]
    #[Assert\NotNull()]
    private string $content;

    public static function of(string $title, string $content): self
    {
        $post = new CreateCommentRequest();

        $post->setContent($content)
             ->setTitle($title);

        return $post;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): CreateCommentRequest
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): CreateCommentRequest
    {
        $this->content = $content;

        return $this;
    }
}
