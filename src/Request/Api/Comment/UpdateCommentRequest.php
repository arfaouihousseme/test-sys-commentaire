<?php

namespace App\Request\Api\Comment;

use App\Trait\ConvertObjectToArrayTrait;

class UpdateCommentRequest
{
    use ConvertObjectToArrayTrait;

    private ?string $content = null;

    public static function of(?string $content = null): self
    {
        $post = new UpdateCommentRequest();

        $post->setContent($content);

        return $post;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(?string $content): UpdateCommentRequest
    {
        $this->content = $content;

        return $this;
    }
}
