<?php

namespace App\Request\Api\Comment;

use Symfony\Component\Validator\Constraints as Assert;

class CreateCommentRequest
{
    #[Assert\NotBlank()]
    #[Assert\NotNull()]
    private string $content;

    public static function of(string $content): self
    {
        $comment = new CreateCommentRequest();

        $comment->setContent($content);

        return $comment;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): CreateCommentRequest
    {
        $this->content = $content;

        return $this;
    }
}
