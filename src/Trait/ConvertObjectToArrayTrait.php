<?php

namespace App\Trait;

trait ConvertObjectToArrayTrait
{
    public function toArray(): array
    {
        $arrayResult = [];
        $reflection = new \ReflectionObject($this);
        $properties = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC | \ReflectionProperty::IS_PRIVATE);

        foreach ($properties as $property) {
            $property->setAccessible(true);
            $property_name = $property->getName();
            $property_value = $property->getValue($this);
            $arrayResult[$property_name] = $property_value;
        }

        return $arrayResult;
    }
}
