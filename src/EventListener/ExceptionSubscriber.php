<?php

namespace App\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ExceptionSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            'kernel.exception' => 'onKernelException',
        ];
    }

    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        $request = $event->getRequest();

        if ('application/json' == $request->headers->get('Content-Type')) {
            if ($exception instanceof HttpExceptionInterface) {
                $response = new JsonResponse([
                    'message' => $exception->getMessage(),
                    'code' => $exception->getStatusCode(),
                ]);
                $response->setStatusCode($exception->getStatusCode());
            } else {
                $response = new JsonResponse([
                    'message' => $exception->getMessage(),
                    'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
                ]);
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            $event->setResponse($response);
        }
    }
}
