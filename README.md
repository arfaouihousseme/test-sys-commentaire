# SY6 rest API

## Local development setup

**Warning:** *Executing this script will reinitialize the database, its data, the parameters and the env file to their factory settings*

### Init project

First checkout the project's source locally inside your workspace directory through:

```bash
git clone git@gitlab.com:arfaouihousseme/test-sys-commentaire.git
cd test-sys-commentaire
```

```bash
make build
```
### Generate a jwt for user

To use api, you must generate a jwt with this command line

```bash
curl --location '127.0.0.1:8080/api/login_check' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username": "user-jwt-test@gmail.com",
    "password": "password123"
}'
```

### Call API

API Swagger doc

```bash
http://127.0.0.1:8080/api/doc
```

Example curl

```bash
To get list of comments

curl --location '127.0.0.1:8080/api/comments' \
--header 'Authorization: bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE2ODEwNTgyOTYsImV4cCI6MTY4MTA2MTg5Niwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjoidXNlci1qd3QtdGVzdEBnbWFpbC5jb20ifQ.LJLFD0T4zoBwj1KS9IhdaHyMVt9DmtwconlLUvHjW1nrSIBvHmQF3x4pT0ma1ke-WdA1fNmPgUfTYkbj-j82tYpaiBwlgdhcUo8-h6NAAaLz3SGUJiUytkztWdgilMmNHdVJN-nVh2tmFVWb3iwTqY8J9pSKZTjUVDmzhCue5Ye6oQcunvKdb0tn84UjaKMSTrGIQURvVjk0BPE5wrexkOKSwBQVuveq15VfwKEpMrOI6n9t72K3maZM6P4wl0O3ukM3jKs7owJUxYL9DCgOkfIaXnL0vKFff7qtR6hd7psI0lQKh7db_KUB3Fy9QYM75htBeXiyHVvyDPkPlXUwasoVBsNB5CTCx5Vo6p6qMS6gLsGQT9EDopOMjsBatYViZ_BLuxnQXUgLdcAma7CklXXNaf6G6Ui0IyZOx9qvN8pzsPlUOjVTphwSRwC3VHBOj-nLgfdwSAyWe6eJCCiAaWfMN8cTQ7075PV4wo05shxYmlbGCHL0sc8SYw6S6DSKXmiqUUFmlEeerLr7yaa0_6pwxyRCVdo1UJEhhPxYTPBmTbIk-0LnxWNgGvlhix8fdTgBUmD0rSyb09DVhxmx6XopGRw5zgzjH2HoZU4d_s7W3IJcyaqrCAgpS9EF09dEBiP2AO2riJSxquuEEd4X1XOi5tN-C5x7Kl7K_HnFtRE' \
--data ''
```